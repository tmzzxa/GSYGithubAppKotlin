package com.shuyu.github.kotlin.module.test.asynctask

import android.os.AsyncTask
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

/**
 * Copyright (C)
 * FileName: DemoAsyncTask
 * Author: zxa
 * Date: 2020/11/7 11:18 PM
 * Description:
 */
class MyAsyncTask(name: String) : AsyncTask<String?, Int?, String>() {
    private var mName = "AsyncTask"
    override fun doInBackground(vararg params: String?): String? {
        try {
            Thread.sleep(3000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return mName
    }

    override fun onPostExecute(s: String) {
        super.onPostExecute(s)
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        Log.e("zxa", s + "execute finish at " + df.format(Date()))
    }

    init {
        mName = name
    }

}

//private fun runAsyncTask() {
////        DemoAsyncTask("task1")
//    Log.i("zxa","runAsyncTask");
//    MyAsyncTask("task1").execute("");
//    MyAsyncTask("task1").execute("");
//    MyAsyncTask("task1").execute("");
//    MyAsyncTask("task1").execute("");
//}